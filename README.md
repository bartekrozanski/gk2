# What is it?

Simple app written using Winforms to learn about 2D images rendering.
It uses, for example, implementation of AET algorithm for polygon filling, simulates directional light, supports normal maps, color blending,
color interpolation from vertices of triangle mesh etc.

# Screenshots
<img src="gk2/screenshots/screenshot1.PNG" width="800" height="450" />
<img src="gk2/screenshots/screenshot2.PNG" width="800" height="450" />
<img src="gk2/screenshots/screenshot3.PNG" width="800" height="450" />

# How to build? 

Clone this repository and launch .sln file with Visual Studio.
