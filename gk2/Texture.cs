﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


using Vertex = System.Numerics.Vector2;
namespace gk2
{
    class Texture
    {
        public static Texture GetUniformTexture(Color color, int width, int height)
        {
            var texture = new Texture(width, height, color);
            return texture;
        }

        public DirectBitmap texture { get; set; }
        public int Width => texture.Width;
        public int Height => texture.Height;
        public Color GetPixel(int x, int y) => texture.GetPixel(x, y);
        public Color GetPixel(Vertex v)
        {
            return texture.GetPixel((int)v.X, (int)v.Y);
        }


        public Texture(Bitmap bitmap)
        {
            texture = new DirectBitmap(bitmap);
        }

        public Texture(int width, int height, Color color)
        {
            texture = new DirectBitmap(width, height);
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    texture.SetPixel(x, y, color);
                }
            }
        }
    }
}
