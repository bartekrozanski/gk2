﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;

using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Numerics;
using Vertex = System.Numerics.Vector2;
namespace gk2
{
   

    class Renderer
    {
        public Lighting Lighting { get; private set; }
        private PolygonFiller PolygonFiller { get; }
        private DirectBitmap[] Bitmaps;
        private DirectBitmap CurrentBitmap { get { return Bitmaps[CurrentBitmapIndex]; } }
        private PictureBox Screen { get; }
        public int Height { get; }
        public int Width { get; }


        private int CurrentBitmapIndex;

        public Renderer(int width, int height, PictureBox screen)
        {
            Screen = screen;
            Width = width;
            Height = height;
            Bitmaps = new DirectBitmap[2];
            Bitmaps[0] = new DirectBitmap(width, height);
            Bitmaps[1] = new DirectBitmap(width, height);
            PolygonFiller = new PolygonFiller(this);
            Lighting = new Lighting();
            CurrentBitmapIndex = 0;
        }

        public void Clear(byte value)
        {
            CurrentBitmap.Clear(value);
        }

        public void Draw(IDrawable drawable)
        {
            drawable.Draw(this);
        }

        public void DrawVertex(Vertex vertex)
        {
            //CurrentBitmap.SetPixel((int)vertex.X, (int)vertex.Y, Color.Black);
            CurrentBitmap.Graphics.DrawEllipse(Pens.Black, vertex.X - 2, vertex.Y - 2, 4, 4);
        }

        public void DrawLine(Vertex a, Vertex b)
        {
            CurrentBitmap.Graphics.DrawLine(Pens.Black, a.X, a.Y, b.X, b.Y);
        }

        public void FillPolygon(List<Vertex> vertices, Texture texture, NormalMap normalMap, float kd, float ks, int m) =>
            PolygonFiller.FillPolygon(vertices, texture, normalMap, kd, ks, m);

        public void FillPolygonInterpolated(List<Vertex> vertices, List<Color> colors, List<Vector3> normalVecs, float kd, float ks, int m) =>
            PolygonFiller.FillPolygonInterpolated(vertices, colors, normalVecs, kd, ks ,m);

        public void FillPolygonHybrid(List<Vertex> vertices, List<Color> colors, List<Vector3> normalVecs, float kd, float ks, int m) =>
    PolygonFiller.FillPolygonHybrid(vertices, colors, normalVecs, kd, ks, m);

        public void Display()
        {
            Screen.Image = CurrentBitmap.Bitmap;
            CurrentBitmapIndex ^= 1;
        }

        public void SetPixel(int x, int y, Color color) => CurrentBitmap.SetPixel(x, y, color);

        public Texture GetUniformTexture(Color color)
        {
            return Texture.GetUniformTexture(color, Width, Height);
        }
    }

    
}

