﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Drawing;
using System.Drawing.Imaging;



using Vertex = System.Numerics.Vector2;
namespace gk2
{

    enum DrawingType
    {
        Exact,
        Interpolated,
        Hybrid,
    }

    enum CoefficientsType
    {
        Constant,
        Random,
    }
    class Mesh : IDrawable
    {

        private Vertex[] Vertices;
        public int Columns { get; }
        public int Rows{ get; }
        public Texture Texture { get; set; }
        public NormalMap NormalMap { get; set; }
        public DrawingType DrawingType { get; set; }
        public CoefficientsType CoefficientsType { get; set; }



        public float kd { get; set; }
        public float ks { get; set; }
        public int m { get; set; }
        private Random rng;
        public int seed;
        public Vertex this[int x, int y]
        {
            get
            {
                return Vertices[y * Columns + x];
            }

            set
            {
                Vertices[y * Columns + x] = value;
            }
        }


        public Mesh(int rows, int columns, Vertex origin, float width, float height, Texture texture, NormalMap normalMap, DrawingType drawingType, CoefficientsType coefficientsType)
        {
            width--;
            height--;
            Columns = columns;
            Rows = rows;
            Vertices = new Vertex[columns* rows];
            for(int i = 0; i < Vertices.Length; ++i)
            {
                Vertices[i] = new Vertex(origin.X + ((i % columns) * width / (columns-1)),
                    origin.Y + ((i / columns) * height / (rows-1)));
            }
            Texture = texture;
            NormalMap = normalMap;
            DrawingType = drawingType;
            CoefficientsType = coefficientsType;
        }

        void IDrawable.Draw(Renderer renderer)
        {
            var vertices = new List<Vertex>();
            var colors = new List<Color>();
            var normalVecs = new List<Vector3>();
            rng = new Random(seed);
            switch (DrawingType)
            {
                case DrawingType.Exact:
                    for (int x = 1; x < Columns; ++x)
                    {
                        for (int y = 0; y < Rows - 1; ++y)
                        {
                            if(CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }

                            vertices.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x - 1, y]);
                            vertices.Add(this[x - 1, y + 1]);
                            renderer.FillPolygon(vertices,
                                Texture,
                                NormalMap,
                                kd, ks, m);
                            if (CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }
                            vertices.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x, y + 1]);
                            vertices.Add(this[x - 1, y + 1]);
                            renderer.FillPolygon(vertices, Texture, NormalMap, kd, ks, m);
                        }
                    }
                    break;

                case DrawingType.Interpolated:
                    for (int x = 1; x < Columns; ++x)
                    {
                        for (int y = 0; y < Rows - 1; ++y)
                        {
                            if (CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }
                            vertices.Clear();
                            colors.Clear();
                            normalVecs.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x - 1, y]);
                            vertices.Add(this[x - 1, y + 1]);
                            colors.Add(Texture.GetPixel(vertices[0]));
                            colors.Add(Texture.GetPixel(vertices[1]));
                            colors.Add(Texture.GetPixel(vertices[2]));
                            normalVecs.Add(NormalMap[vertices[0]]);
                            normalVecs.Add(NormalMap[vertices[1]]);
                            normalVecs.Add(NormalMap[vertices[2]]);
                            renderer.FillPolygonInterpolated(vertices, colors, normalVecs, kd ,ks ,m);

                            if (CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }
                            vertices.Clear();
                            colors.Clear();
                            normalVecs.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x, y + 1]);
                            vertices.Add(this[x - 1, y + 1]);
                            colors.Add(Texture.GetPixel(vertices[0]));
                            colors.Add(Texture.GetPixel(vertices[1]));
                            colors.Add(Texture.GetPixel(vertices[2]));
                            normalVecs.Add(NormalMap[vertices[0]]);
                            normalVecs.Add(NormalMap[vertices[1]]);
                            normalVecs.Add(NormalMap[vertices[2]]);
                            renderer.FillPolygonInterpolated(vertices, colors, normalVecs, kd, ks ,m);
                        }
                    }
                    break;

                case DrawingType.Hybrid:
                    for (int x = 1; x < Columns; ++x)
                    {
                        for (int y = 0; y < Rows - 1; ++y)
                        {
                            if (CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }
                            vertices.Clear();
                            colors.Clear();
                            normalVecs.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x - 1, y]);
                            vertices.Add(this[x - 1, y + 1]);
                            colors.Add(Texture.GetPixel(vertices[0]));
                            colors.Add(Texture.GetPixel(vertices[1]));
                            colors.Add(Texture.GetPixel(vertices[2]));
                            normalVecs.Add(NormalMap[vertices[0]]);
                            normalVecs.Add(NormalMap[vertices[1]]);
                            normalVecs.Add(NormalMap[vertices[2]]);
                            renderer.FillPolygonHybrid(vertices, colors, normalVecs, kd, ks, m);

                            if (CoefficientsType == CoefficientsType.Random)
                            {
                                kd = (float)rng.NextDouble();
                                ks = (float)rng.NextDouble();
                                m = rng.Next(100);
                            }
                            vertices.Clear();
                            colors.Clear();
                            normalVecs.Clear();
                            vertices.Add(this[x, y]);
                            vertices.Add(this[x, y + 1]);
                            vertices.Add(this[x - 1, y + 1]);
                            colors.Add(Texture.GetPixel(vertices[0]));
                            colors.Add(Texture.GetPixel(vertices[1]));
                            colors.Add(Texture.GetPixel(vertices[2]));
                            normalVecs.Add(NormalMap[vertices[0]]);
                            normalVecs.Add(NormalMap[vertices[1]]);
                            normalVecs.Add(NormalMap[vertices[2]]);
                            renderer.FillPolygonHybrid(vertices, colors, normalVecs, kd, ks, m);
                        }
                    }
                    break;
            }
            
            for (int i = 0; i < Vertices.Length; ++i)
            {
                renderer.DrawVertex(Vertices[i]);
            }
            for(int x = 1; x < Columns; ++x)
            {
                for(int y = 0; y < Rows - 1; ++y)
                {
                    renderer.DrawLine(this[x, y], this[x, y + 1]);
                    renderer.DrawLine(this[x, y], this[x-1, y]);
                    renderer.DrawLine(this[x, y], this[x - 1, y + 1]);
                }
                renderer.DrawLine(this[x,Rows-1], this[x-1, Rows-1]);
            }
            for(int y = 0; y < Rows -1; ++y)
            {
                renderer.DrawLine(this[0, y], this[0, y + 1]);
            }

        }
    }
}
