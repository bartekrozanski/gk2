﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gk2
{
    class Node<T>
    {
        public Node(T val, Node<T> prev, Node<T> next)
        {
            Prev = prev;
            Next = next;
            Value = val;
        }
        public Node<T> Next;
        public Node<T> Prev;
        public T Value;
    }

    class CustomLinkedList<T>
    {
        public Node<T> First;
        public Node<T> Last;


        public static CustomLinkedList<T> Merge(CustomLinkedList<T> listA, CustomLinkedList<T> listB, Func<T, IComparable> extractor)
        {
            var newList = new CustomLinkedList<T>();

            var itA = listA.First;
            var itB = listB.First;
            while(itA != null && itB != null)
            {
                if(extractor(itA.Value).CompareTo(extractor(itB.Value)) < 0)
                {
                    itA = newList.AddLast(itA);
                }
                else
                {
                    itB = newList.AddLast(itB);
                }
            }
            while(itA != null)
            {
                itA = newList.AddLast(itA);
            }
            while(itB != null)
            {
                itB = newList.AddLast(itB);
            }
            return newList;
        }

        public CustomLinkedList()
        {
            First = null;
            Last = null;
        }

        public void AddLast(T val)
        {
            if(First == null)
            {
                First = Last = new Node<T>(val, null, null);
            }
            else
            {
                Last.Next = new Node<T>(val, Last, null);
                Last = Last.Next;
            }
        }

        public void AddFirst(T val)
        {
            if (First == null)
            {
                First = Last = new Node<T>(val, null, null);
            }
            else
            {
                First.Prev = new Node<T>(val, null, First);
                First = First.Prev;
            }
        }

        public void InsertSorted(T val, Func<T, IComparable> extractor)
        {
            var it = First;
            while(it != null && extractor(it.Value).CompareTo(extractor(val)) < 0)
            {
                it = it.Next;
            }
            if(it == null)
            {
                AddLast(val);
            }
            else
            {
                if(it == First)
                {
                    AddFirst(val);
                }
                else
                {
                    it.Prev.Next = new Node<T>(val, it.Prev, it);
                }
            }
        }

        public Node<T> Remove(Node<T> node)
        {
            var left = node.Prev;
            var right = node.Next;
            if (left != null)
            {
                left.Next = right;
            }
            if(right != null)
            {
                right.Prev = left;
            }
            if(node == Last)
            {
                Last = left;
            }
            if(node ==First)
            {
                First = right;
            }
            return right;
        }



        public Node<T> AddLast(Node<T> node)
        {
            var next = node.Next;
            if (First == null)
            {
                First = Last = node;
                node.Prev = node.Next = null;
            }
            else
            {
                node.Prev = Last;
                node.Next = null;
                Last.Next = node;
                Last = node;
            }
            return next;
        }
    }
}
