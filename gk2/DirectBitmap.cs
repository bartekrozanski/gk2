﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Reflection.Emit;
using System.Reflection;

namespace gk2
{
    public class DirectBitmap : IDisposable
    {
        public Graphics Graphics { get; }
        public Bitmap Bitmap { get; private set; }
        public Int32[] RawData { get; private set; }
        public bool Disposed { get; private set; }
        public int Height { get; }
        public int Width { get; }
        public int BytesPerPixel { get; }

        protected GCHandle RawDataHandle;



        public DirectBitmap(int width, int height)
        {
            Width = width;
            Height = height;
            BytesPerPixel = 4;
            RawData = new Int32[width * height];
            RawDataHandle = GCHandle.Alloc(RawData, GCHandleType.Pinned);
            Bitmap = new Bitmap(width,
                height,
                width * BytesPerPixel,
                PixelFormat.Format32bppArgb,
                RawDataHandle.AddrOfPinnedObject());
            Graphics = Graphics.FromImage(Bitmap);
        }

        public DirectBitmap(Bitmap bitmap)
        {
            Width = bitmap.Width;
            Height = bitmap.Height;
            BytesPerPixel = 4;
            RawData = new Int32[Width * Height];
            RawDataHandle = GCHandle.Alloc(RawData, GCHandleType.Pinned);
            Bitmap = new Bitmap(Width,
                Height,
                Width * BytesPerPixel,
                PixelFormat.Format32bppArgb,
                RawDataHandle.AddrOfPinnedObject());
            Graphics = Graphics.FromImage(Bitmap);
            for (int x = 0; x < Width; ++x)
            {
                for (int y = 0; y < Height ; ++y)
                {
                    SetPixel(x, y, bitmap.GetPixel(x, y));
                }
            }
        }

        public void SetPixel(int x, int y, Color color)
        {
            int index = x + y * Width;
            RawData[index] = color.ToArgb();
        }

        public Color GetPixel(int x, int y) => Color.FromArgb(RawData[x + y*Width]);

        public void Fill(Color color)
        {
            for (int x = 0; x < Width; ++x)
            {
                for (int y = 0; y < Height; ++y)
                {
                    SetPixel(x, y, color);
                }
            }
        }

        public void Clear(byte value)
        {
            Util.Memset(RawDataHandle, value, RawData.Length * BytesPerPixel);
        }


        public void Dispose()
        {
            if (Disposed) return;
            Disposed = true;
            Bitmap.Dispose();
            RawDataHandle.Free();
            Graphics.Dispose();
        }
    }

    public static class Util
    {
        static Util()
        {
            var dynamicMethod = new DynamicMethod("Memset", MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard,
                null, new[] { typeof(IntPtr), typeof(byte), typeof(int) }, typeof(Util), true);

            var generator = dynamicMethod.GetILGenerator();
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldarg_1);
            generator.Emit(OpCodes.Ldarg_2);
            generator.Emit(OpCodes.Initblk);
            generator.Emit(OpCodes.Ret);

            MemsetDelegate = (Action<IntPtr, byte, int>)dynamicMethod.CreateDelegate(typeof(Action<IntPtr, byte, int>));
        }

        public static void Memset(GCHandle handleToArr, byte what, int length)
        {
            MemsetDelegate(handleToArr.AddrOfPinnedObject(), what, length);
        }

        private static Action<IntPtr, byte, int> MemsetDelegate;
    }
}
