﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Drawing;
using System.Drawing.Imaging;

namespace gk2
{
    class Lighting
    {
        public Vector3 L { get; set; }
        Vector3 V { get; set; }
        public Vector3 LightColor { get; set; }




        public Lighting()
        {
            L = Vector3.UnitZ;
            V = Vector3.UnitZ;
            LightColor = Vector3.One;
        }



        public Color OutputColor(float kd, float ks, float m, Vector3 color, Vector3 N)
        {
            Vector3 R = 2 * Vector3.Dot(N, L) * N - L;
            Vector3 lambert; 
            Vector3 mirror;;
            var cosNL = Vector3.Dot(N, L);
            var cosVR = Vector3.Dot(V, Vector3.Normalize(R));

            if (cosNL < 0) lambert = Vector3.Zero;
            else
            {
                lambert = kd * LightColor * color * cosNL;
            }
            if (cosVR < 0) mirror = Vector3.Zero;
            else
            {
                mirror = ks * LightColor * color * (float)Math.Pow(cosVR, m);
            }
            var mirrorClamped = Vector3.Clamp(mirror, Vector3.Zero, mirror);
            Vector3 output = lambert + mirror;
            output = Vector3.Clamp(output, Vector3.Zero, new Vector3(255));

            return Color.FromArgb((int)output.X, (int)output.Y, (int)output.Z);
        }

        public Color OutputColor(float kd, float ks, float m, Color color, Vector3 N)
        {
            return OutputColor(kd, ks, m, new Vector3(color.R, color.G, color.B), N);
        }
    }
}
