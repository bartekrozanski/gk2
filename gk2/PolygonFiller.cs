﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Numerics;
using Vertex = System.Numerics.Vector2;
using Texture = System.Drawing.Bitmap;

namespace gk2
{

    struct AETStruct
    {
        public int ymax;
        public float x;
        public float dydx;

        public AETStruct(int ymax, float x, float dydx)
        {
            this.ymax = ymax;
            this.x = x;
            this.dydx = dydx;
        }
    }


    class PolygonFiller
    {
        List<AETStruct>[] ET { get; set; }
        int ETHighest { get; set; }
        int ETLowest { get; set; }

        Renderer Renderer { get; }
        public PolygonFiller(Renderer renderer)
        {
            Renderer = renderer;
            ET = new List<AETStruct>[Renderer.Height];
        }

        private void UpdateET(List<Vertex> vertices)
        {
            Array.Clear(ET, 0, ET.Length);
            ETLowest = int.MaxValue;
            ETHighest = int.MinValue;

            var edges = VerticesToEdges(vertices);
            for(int i = 0; i < edges.Count; ++i)
            {
                var edge = edges[i];

                if ((int)edge.Item1.Y == (int)edge.Item2.Y) continue; // horizontal lines are skipped

                int yMin = (int)Math.Min(edge.Item1.Y, edge.Item2.Y);
                if (ET[yMin] == null) ET[yMin] = new List<AETStruct>();
                var node = EdgeToAETNode(edge);
                ET[yMin].Add(node);
                ETLowest = Math.Min(ETLowest, yMin);
                ETHighest= Math.Max(ETHighest, node.ymax);
            }
        }

        private AETStruct EdgeToAETNode((Vertex, Vertex) edge)
        {
            float ymax;
            float x;
            float dx;
            float dy;
            if(edge.Item1.Y > edge.Item2.Y)
            {
                ymax = edge.Item1.Y;
                x = edge.Item2.X;
                dy = ymax - edge.Item2.Y;
                dx = edge.Item1.X - edge.Item2.X;

            }
            else
            {
                ymax = edge.Item2.Y;
                x = edge.Item1.X;
                dy = ymax - edge.Item1.Y;
                dx = edge.Item2.X - edge.Item1.X;
            }

            return new AETStruct((int)ymax, x, dx / dy);
        }

        private List<(Vertex, Vertex)> VerticesToEdges(List<Vertex> vertices)
        {
            var edges = new List<(Vertex, Vertex)>();
            for(int i = 0; i< vertices.Count - 1; ++i)
            {
                edges.Add((vertices[i], vertices[i + 1]));
            }
            edges.Add((vertices[vertices.Count - 1], vertices[0]));
            return edges;
        }

        private Color InterpolateColor(List<Vertex> v, Vertex p, List<Color> colors)
        {
            float W1 = (((v[1].Y - v[2].Y) * (p.X - v[2].X)) + ((v[2].X - v[1].X) * (p.Y - v[2].Y)))
                / (((v[1].Y - v[2].Y) * (v[0].X - v[2].X)) + ((v[2].X - v[1].X) * (v[0].Y - v[2].Y)));
            float W2 = (((v[2].Y - v[0].Y) * (p.X - v[2].X)) + ((v[0].X - v[2].X) * (p.Y - v[2].Y)))
    / (((v[1].Y - v[2].Y) * (v[0].X - v[2].X)) + ((v[2].X - v[1].X) * (v[0].Y - v[2].Y)));
            float W3 = 1 - W1 - W2;
            int R = (int)(colors[0].R * W1 + colors[1].R * W2 + colors[2].R * W3);
            int G = (int)(colors[0].G * W1 + colors[1].G * W2 + colors[2].G * W3);
            int B = (int)(colors[0].B * W1 + colors[1].B * W2 + colors[2].B * W3);
            if (R < 0) R = 0;
            if (G < 0) G = 0;
            if (B < 0) B = 0;
            if (R > 255) R = 255;
            if (G > 255) G = 255;
            if (B > 255) B = 255;
            return Color.FromArgb(R, G, B);
        }

        private Vector3 InterpolateVector(List<Vertex> v, Vertex p, List<Vector3> vecs)
        {
            float W1 = (((v[1].Y - v[2].Y) * (p.X - v[2].X)) + ((v[2].X - v[1].X) * (p.Y - v[2].Y)))
                / (((v[1].Y - v[2].Y) * (v[0].X - v[2].X)) + ((v[2].X - v[1].X) * (v[0].Y - v[2].Y)));
            float W2 = (((v[2].Y - v[0].Y) * (p.X - v[2].X)) + ((v[0].X - v[2].X) * (p.Y - v[2].Y)))
    / (((v[1].Y - v[2].Y) * (v[0].X - v[2].X)) + ((v[2].X - v[1].X) * (v[0].Y - v[2].Y)));
            float W3 = 1 - W1 - W2;

            return Vector3.Normalize(W1 * vecs[0] + W2 * vecs[1] + W3 * vecs[2]);
        }

        public void FillPolygonInterpolated(List<Vertex> vertices, List<Color> colors, List<Vector3> normalVecs, float kd, float ks, int m)
        {
            var outputColors = new List<Color>();
            for(int i = 0; i < vertices.Count; ++i)
            {
                outputColors.Add(Renderer.Lighting.OutputColor(kd, ks, m, colors[i], normalVecs[i]));
            }
            UpdateET(vertices);
            int y = ETLowest;
            var AET = new List<AETStruct>();
            while (y <= ETHighest || AET.Count > 0)
            {
                if (ET[y] != null)
                {
                    AET = new List<AETStruct>(AET.Concat(ET[y]));
                }
                AET = new List<AETStruct>(AET.OrderBy(node => node.x));
                for (int i = 0; i < AET.Count; i += 2)
                {
                    for (int x = (int)AET[i].x; x < (int)AET[i + 1].x; ++x)
                    {
                        var color = InterpolateColor(vertices, new Vertex(x, y), outputColors);
                        Renderer.SetPixel(x, y, color);
                    }
                }
                y++;
                AET = new List<AETStruct>(AET.Where(node => node.ymax != y));
                for (int i = 0; i < AET.Count; ++i)
                {
                    var current = AET[i];
                    AET[i] = new AETStruct(current.ymax, current.x + current.dydx, current.dydx);
                }
            }
        }

        public void FillPolygonHybrid(List<Vertex> vertices, List<Color> colors, List<Vector3> normalVecs, float kd, float ks, int m)
        {
            UpdateET(vertices);
            int y = ETLowest;
            var AET = new List<AETStruct>();
            while (y <= ETHighest || AET.Count > 0)
            {
                if (ET[y] != null)
                {
                    AET = new List<AETStruct>(AET.Concat(ET[y]));
                }
                AET = new List<AETStruct>(AET.OrderBy(node => node.x));
                for (int i = 0; i < AET.Count; i += 2)
                {
                    for (int x = (int)AET[i].x; x < (int)AET[i + 1].x; ++x)
                    {
                        var interpolatedVec = InterpolateVector(vertices, new Vertex(x, y), normalVecs);
                        var interpolatedColor = InterpolateColor(vertices, new Vertex(x, y), colors);
                        var outputColor = Renderer.Lighting.OutputColor(kd, ks, m, interpolatedColor, interpolatedVec);
                        Renderer.SetPixel(x, y, outputColor);
                    }
                }
                y++;
                AET = new List<AETStruct>(AET.Where(node => node.ymax != y));
                for (int i = 0; i < AET.Count; ++i)
                {
                    var current = AET[i];
                    AET[i] = new AETStruct(current.ymax, current.x + current.dydx, current.dydx);
                }
            }
        }


        public void FillPolygon(List<Vertex> vertices, Texture texture, NormalMap normalMap, float kd, float ks, int m)
        {
            UpdateET(vertices);
            int y = ETLowest;
            var AET = new List<AETStruct>();
            while (y <= ETHighest || AET.Count > 0)
            {
                if (ET[y] != null)
                {
                    AET = new List<AETStruct>(AET.Concat(ET[y]));
                }
                AET = new List<AETStruct>(AET.OrderBy(node => node.x));
                for (int i = 0; i < AET.Count; i += 2)
                {
                    for(int x = (int)AET[i].x; x < (int)AET[i+1].x; ++x)
                    {
                        var color = Renderer.Lighting.OutputColor(kd, ks, m, texture.GetPixel(x, y), normalMap[x, y]);
                        Renderer.SetPixel(x, y, color);

                    }
                }
                y++;
                AET = new List<AETStruct>(AET.Where(node => node.ymax != y));
                for(int i = 0; i < AET.Count; ++i)
                {
                    var current = AET[i];
                    AET[i] = new AETStruct(current.ymax, current.x + current.dydx, current.dydx);
                }
            }
        }
    }
}
