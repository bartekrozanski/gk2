﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Numerics;

using Vertex = System.Numerics.Vector2;
using Texture = System.Drawing.Bitmap;
namespace gk2
{
    public partial class Form1 : Form
    {

        public static List<Color> ColorStructToList()
        {
            return typeof(Color).GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public)
                                .Select(c => (Color)c.GetValue(null, null))
                                .ToList();
        }


        Renderer Renderer { get; }
        Mesh Mesh { get; set; }
        private int clickedVertexX;
        private int clickedVertexY;
        private float dt = 0.06f;
        private float t = 0;
        private float s = 0;
        private float ds = 0.00f;



        public Form1()
        {
            InitializeComponent();
            Renderer = new Renderer(pictureBox2.Width, pictureBox2.Height, pictureBox2);
            Mesh = new Mesh(
                10,
                15,
                new Vertex(0, 0),
                Renderer.Width,
                Renderer.Height,
                Renderer.GetUniformTexture(Color.White),
                new NormalMap(Renderer.GetUniformTexture(Color.FromArgb(0x7f, 0x7f, 0xff))),
                DrawingType.Exact,
                CoefficientsType.Constant
                );
            listBox1.Items.Add("<Texture>");
            foreach (var color in ColorStructToList())
            {
                listBox1.Items.Add(color);
                listBox2.Items.Add(color);
            }
            radioButton1.Select();

            Draw();
        }

        private void Draw()
        {
            Renderer.Clear(255);
            Renderer.Draw(Mesh);
            Renderer.Display();
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;
            if (e.X < 0) x = 0;
            if (e.X >= pictureBox2.Width) x = pictureBox2.Width-1;
            if (e.Y < 0) y = 0;
            if (e.Y >= pictureBox2.Height) y = pictureBox2.Height-1;

            Mesh[clickedVertexX, clickedVertexY] = new Vertex(x, y);
            Draw();
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            Vertex click = new Vertex(e.X, e.Y);
            for(int x = 0 ; x < Mesh.Columns; ++x)
            {
                for(int y = 0; y < Mesh.Rows; ++y)
                {
                    if(CollidedWithVertex(click, Mesh[x,y]))
                    {
                        clickedVertexX = x;
                        clickedVertexY = y;
                        pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(pictureBox2_MouseMove);
                        return;
                    }
                }
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox2.MouseMove -= new System.Windows.Forms.MouseEventHandler(pictureBox2_MouseMove);
        }

        private bool CollidedWithVertex(Vertex click, Vertex vertex)
        {
            if((click - vertex).LengthSquared() < 50)
            {
                return true;
            }
            return false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Mesh = new Mesh((int)numericUpDown1.Value, (int)numericUpDown2.Value, new Vertex(0, 0), Renderer.Width, Renderer.Height, Mesh.Texture, Mesh.NormalMap, Mesh.DrawingType, Mesh.CoefficientsType);
            Draw();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Mesh = new Mesh((int)numericUpDown1.Value, (int)numericUpDown2.Value, new Vertex(0, 0), Renderer.Width, Renderer.Height, Mesh.Texture, Mesh.NormalMap, Mesh.DrawingType, Mesh.CoefficientsType);
            Draw();

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem is string)
            {
                using (var selectFileDialog = new OpenFileDialog())
                {
                    if (selectFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        var texture = new Texture(new Bitmap(Image.FromFile(selectFileDialog.FileName), new Size(Renderer.Width, Renderer.Height)));
                        Mesh.Texture = texture;
                    }
                }
            }
            else
            {
                DirectBitmap newUniformTexture = new DirectBitmap(Renderer.Width, Renderer.Height);
                var selectedColor = (Color)listBox1.SelectedItem;
                Mesh.Texture = Renderer.GetUniformTexture(selectedColor);
            }
            Draw();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                Mesh.NormalMap = new NormalMap(Renderer.GetUniformTexture(Color.FromArgb(0x7f, 0x7f, 0xff)));
            }
            Draw();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if(radioButton.Checked)
            {
                using (var selectFileDialog = new OpenFileDialog())
                {
                    if (selectFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        var nomralMap = new NormalMap(
                            new Texture(new Bitmap(Image.FromFile(selectFileDialog.FileName), new Size(Renderer.Width, Renderer.Height)))
                        );
                        Mesh.NormalMap = nomralMap;
                    }
                }
            }
            Draw();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Mesh.kd = trackBar1.Value / 100f;
            Mesh.CoefficientsType = CoefficientsType.Constant;
            label4.Text = Mesh.kd.ToString();
            Draw();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Mesh.ks = trackBar2.Value / 100f;
            Mesh.CoefficientsType = CoefficientsType.Constant;
            label5.Text = Mesh.ks.ToString();
            Draw();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            Mesh.m = trackBar3.Value;
            Mesh.CoefficientsType = CoefficientsType.Constant;
            label6.Text = Mesh.m.ToString();
            Draw();
        }
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            Mesh.DrawingType = DrawingType.Hybrid;
            Draw();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            Mesh.DrawingType = DrawingType.Interpolated;
            Draw();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Mesh.DrawingType = DrawingType.Exact;
            Draw();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            timer1.Stop();
            Renderer.Lighting.L = Vector3.UnitZ;
            Draw();
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            timer1.Interval = 30;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            t += dt;
            s += ds;
            Renderer.Lighting.L = new Vector3((float)Math.Cos(t), (float)Math.Cos(s), (float)Math.Sin(t));
            if (t > 3.14) dt *= -1;
            if (t < 0) dt *= -1;
            if (s > 3.14) ds *= -1;
            if (s < 0) ds *= -1;
            Draw();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Mesh.seed = new Random().Next();
            Mesh.CoefficientsType = CoefficientsType.Random;
            Draw();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var color = (Color)listBox2.SelectedItem;
            Renderer.Lighting.LightColor = new Vector3(color.R / 255f, color.G / 255f, color.B / 255);
            Draw();
        }
    }
}
