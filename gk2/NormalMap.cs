﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;
using Vertex = System.Numerics.Vector2;
namespace gk2
{
    class NormalMap
    {
        public static Vector3 ColorToNormalVector(Color color)
        {
            var afloat = ByteToNormalized(color.R);
            var bfloat = ByteToNormalized(color.G);
            var cfloat = ByteToNormalized(color.B);
            return new Vector3(afloat, bfloat, cfloat);
        }

        public static float ByteToNormalized(int val)
        {
            return (2 / 255.0f) * val - 1;
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public Vector3[] Map { get; }

        public Vector3 this[int x, int y]
        {
            get
            {
                return Map[y * Width + x];
            }
            set
            {
                Map[y*Width + x] = value;
            }
        }

        public Vector3 this[Vertex v]
        {
            get
            {
                return this[(int)v.X, (int)v.Y];
            }

        }

        public NormalMap(Bitmap bitmap)
        {
            Width = bitmap.Width;
            Height = bitmap.Height;
            Map = new Vector3[Width * Height];
            for(int x = 0; x < Width; ++x)
            {
                for(int y = 0; y < Height; ++y)
                {
                    var color = bitmap.GetPixel(x, y);
                    this[x, y] = ColorToNormalVector(color);
                }
            }
        }

        public NormalMap(Texture texture)
        {
            Width = texture.Width;
            Height = texture.Height;
            Map = new Vector3[Width * Height];
            for (int x = 0; x < Width; ++x)
            {
                for (int y = 0; y < Height; ++y)
                {
                    var color = texture.GetPixel(x, y);
                    this[x, y] = ColorToNormalVector(color);
                }
            }
        }
    }
}
